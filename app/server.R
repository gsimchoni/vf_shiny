# Load render functions ----
source("renderers.R")


# Server Function ----
server <- function(input, output, session) {
  
  # showTypeInfoFunc <- reactive(input$showTypeInfoFunc)
  # showTypeInfoVar <- reactive(input$showTypeInfoVar)
  
  edges_type <- reactive(input$edges_type)

  output$clusters <- renderClustersGraph()
  
  method_edges_for_func_graph <- reactive({
    if (edges_type() == "call") {
      method_edges %>%
        filter(caller == 1)
    } else if (edges_type() == "var") {
      method_edges %>%
        filter(var == 1)
    } else {
      method_edges
    }
  })
 
  output$test <- renderText(nrow(method_edges_for_func_graph()))
  
  output$functions <- renderVisNetwork(func_graph(method_edges_for_func_graph()))
  
  observe({
    if (input$focus == "None") {
      visNetworkProxy("functions") %>%
        visFit()
    } else {
      visNetworkProxy("functions") %>%
        visFocus(input$focus, scale = 3)
    }
  })
  
  observe({
    if (input$select_method_id == "None") {
      visNetworkProxy("functions") %>%
        visSelectNodes(id = NULL)
    } else {
      visNetworkProxy("functions") %>%
        visSelectNodes(input$select_method_id)
    }
  })
  
  vals <- reactiveValues(sel=FALSE)
  
  observeEvent(input$select_method_id, {
    vals$sel <- TRUE
  })
  
  observeEvent(input$current_node_id, {
    vals$sel <- FALSE
  })
  
  method_info <- eventReactive(c(vals$sel, input$select_method_id, input$current_node_id), {
    if (vals$sel) {
      if (!is.null(input$select_method_id)) {
        method_info <- get_method_info(input$select_method_id)
        make_method_info_text(method_info)
      }
    } else {
      if (!is.null(input$current_node_id$nodes[[1]])) {
        method_info <- get_method_info(input$current_node_id$nodes[[1]])
      }
    }
    make_method_info_text(method_info)
  })
  
  output$selected_method_info <- renderText({
    method_info()
  })
  
  output$entry_points <- renderEntryPointsTable()
  
  output$clusters_general <- renderClustersGeneralTable()
  
  output$tf_idf <- renderTfIdfTable()
  
  output$functions_general_state <- renderFunctionsStateTable()
  
  output$functions_general_n_nodes <- renderFunctionsNNodesTable()
  
  output$functions_general_vars <- renderFunctionsVarsTable()
  
  output$functions_general_cpu <- renderFunctionsCPUTable()
  
  output$vars_general_type <- renderVarsTypeTable()
  
  output$vars_general_access <- renderVarsAccessTable()
  
  output$vars_general_n_methods <- renderVarsNMethodsTable()
  
  output$list_all <- renderListAllTable(input)
  
  observe_focus_entity_in_compare(input, session)
  
  output$compare <- renderCompareTable(input)
  
  output$func_in_tree <- renderFuncInTreeTable(input)
  
}