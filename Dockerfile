FROM rocker/shiny-verse:latest

RUN R -e "install.packages('visNetwork', repos = 'http://cran.us.r-project.org')"
RUN R -e "install.packages('janitor', repos = 'http://cran.us.r-project.org')"
RUN R -e "install.packages('DT', repos = 'http://cran.us.r-project.org')"

COPY app /srv/shiny-server/app
COPY shiny-server-custom.conf /etc/shiny-server/shiny-server.conf

EXPOSE 3838